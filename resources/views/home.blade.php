@extends('layouts.app')

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Subsciption List') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        @foreach($users as $u)
                        <tr>
                            <th>{{ $u->name }}</th>
                            <th>{{ $u->email }}</th>
                            <th>
                                @if($u->status == '1') Active
                                @else Inactive
                                @endif       
                            </th>
                            <th>
                                @if($u->status == '1')
                                    <a href="">Inactivate</a>
                                @else
                                    <a href="">Activate</a>
                                @endif 
                            </th>
                        </tr>
                        @endforeach
                    </tfoot>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endpush