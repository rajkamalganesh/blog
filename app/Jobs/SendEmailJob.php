<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            Mail::send([], [], function($message)
            {
                $message->from("blogt85@gmail.com", "Blog");
                $message->to($this->details['email']);
                $message->subject('Approved your blog123');
                $message->setBody('Hello', 'text/html');
            });
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }
    }
}
