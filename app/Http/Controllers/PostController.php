<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Mail;
use App\Jobs\SendEmailJob;
use Cache;
use DB;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function list()
    {
        //Cache::forget('posts');
        if(Cache::has('posts')){
            $allPosts = json_decode(Cache::get("posts"), true); 
            $posts = [];
            if(Auth::user()->user_type == 2)
            {
                foreach ($allPosts as $key => $value) {
                   if(Auth::user()->id == $value['created_by']){
                        $posts[] = $value;
                   }
                }
            }else{
                $posts = $allPosts;
            }
            $from = 'Cache';
        }else{
            if(Auth::user()->user_type == 2)
                $posts = Post::where("created_by", '=', Auth::user()->id)->get();
            else
                $posts = Post::get();
            $from = 'DB';
        }
        return view('post.list')->with(["posts" => $posts, "from" => $from]);
    }

    public function create_post()
    {
        return view('post.create');
    }

    public function call_create_post(Request $data)
    {
        Post::create([
            'title' => $data->title,
            'description' => $data->description,
            'created_by' => Auth::user()->id
        ]);

        $existing_posts = [];
        if(Cache::has('posts')){
            Cache::forget('posts');
        }

        $allPosts = DB::table('posts')->get();
        if(count($allPosts) > 0){
            foreach($allPosts as $post){
                $existing_posts[] = (array)$post;
            }
            Cache::forever('posts', json_encode($existing_posts));
        }

        \Illuminate\Support\Facades\Session::flash('status', "Successfully Created");
        return Redirect::back();
    }

    public function post_approve($id)
    {
        $post = Post::find($id);
        $post->status = '1';
        $post->approved_by = Auth::user()->id;
        $post->approved_on = date("Y-m-d h:i:s");
        $post->save();

        $existing_posts = [];
        if(Cache::has('posts')){
            Cache::forget('posts');
        }

        $allPosts = DB::table('posts')->get();
        if(count($allPosts) > 0){
            foreach($allPosts as $post){
                $existing_posts[] = (array)$post;
            }
            Cache::forever('posts', json_encode($existing_posts));
        }

        $user = User::where('status', '=', '1')->get();
        foreach ($user as $u) {
            $details['email'][] = $u->email;
        }
        dispatch(new SendEmailJob($details));

        return Redirect::back();
    }
}
