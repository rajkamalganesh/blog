<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->user_type == '1')
        {
            $Mdlusers = new User();
            $users = $Mdlusers->where('user_type', '!=', '1')->get();
            return view('home')->with(["users" => $users]);
        }else{
            return redirect('/create_post');
        }
    }
}
