<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/post', 'PostController@list')->name('post');
Route::get('/create_post', 'PostController@create_post')->name('create_post');
Route::post('/call_create_post', 'PostController@call_create_post')->name('call_create_post');
Route::get('/post_approve/{id?}', 'PostController@post_approve');